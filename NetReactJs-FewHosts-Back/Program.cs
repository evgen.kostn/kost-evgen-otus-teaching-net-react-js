var builder = WebApplication.CreateBuilder(args);

//--- CORS

ConfigurationManager configuration = builder.Configuration;
string Origin = "MyAllowSpecificOrigins";

builder.Services.AddCors(options =>
{
    var corsOrigins = configuration.GetSection("CORS:Origins").Get<string[]>();
    var corsHeaders = configuration.GetSection("CORS:Headers").Get<string[]>();
    var corsMethods = configuration.GetSection("CORS:Methods").Get<string[]>();

    if ( corsOrigins is not null && corsHeaders is not null && corsMethods is not null )
        options.AddPolicy(name: Origin,
                          builder =>
                          {
                              builder.WithOrigins(corsOrigins)
                                     .WithHeaders(corsHeaders)
                                     .WithMethods(corsMethods);
                          });
});

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

//---

// Configure the HTTP request pipeline.
if ( app.Environment.IsDevelopment() )
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();

app.UseCors(Origin);

app.MapControllers();

app.Run();