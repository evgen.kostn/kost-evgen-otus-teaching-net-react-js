using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using System.Runtime.CompilerServices;
using System.Runtime.Intrinsics.X86;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllersWithViews();

//--- CORS

ConfigurationManager configuration = builder.Configuration;
string Origin = "MyAllowSpecificOrigins";

builder.Services.AddCors(options =>
{
    var corsOrigins = configuration.GetSection("CORS:Origins").Get<string[]>();
    var corsHeaders = configuration.GetSection("CORS:Headers").Get<string[]>();
    var corsMethods = configuration.GetSection("CORS:Methods").Get<string[]>();

    if ( corsOrigins is not null && corsHeaders is not null && corsMethods is not null )
        options.AddPolicy(name: Origin,
                          builder =>
                          {
                              builder.WithOrigins(corsOrigins)
                              .WithHeaders(corsHeaders)
                              .WithMethods(corsMethods);
                          });
});

builder.Services.AddSpaStaticFiles(configuration =>
{
    configuration.RootPath = "ClientApp/build";
});

//---

var app = builder.Build();

//--- SPA

app.UseSpa(spa =>
{
    spa.Options.SourcePath = "ClientApp";

    if ( app.Environment.IsDevelopment() )
    {
        spa.UseProxyToSpaDevelopmentServer("http://localhost:5001/");
        spa.UseReactDevelopmentServer(npmScript: "start");
    }
    else
    {
        app.MapFallbackToFile("index.html");
    }
});

//---

// Configure the HTTP request pipeline.
if ( !app.Environment.IsDevelopment() )
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
    //app.UseHttpsRedirection();
}

app.UseStaticFiles();
app.UseSpaStaticFiles();
app.UseRouting();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.Run();