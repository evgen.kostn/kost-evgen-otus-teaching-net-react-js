import {useState} from 'react';

const App = () => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [err, setErr] = useState('');

  const handleClick = async () => {
    setIsLoading(true);

    try {
      const response = await fetch(' http://localhost:3001/WeatherForecast', {
        method: 'GET',
        headers: {
          Accept: 'application/json',
		  'Access-Control-Allow-Origin': '*'
        },
      });

      if (!response.ok) {
        throw new Error(`Error! status: ${response.status}`);
      }

      const result = await response.json();

      console.log('result is: ', JSON.stringify(result, null, 4));

      setData(result);
    } catch (err) {
      setErr(err.message);
    } finally {
      setIsLoading(false);
    }
  };

  console.log(data);

  return (
    <div>
      {err && <h2>{err}</h2>}

      <button onClick={handleClick}>Get weather</button>
          <div>
            <pre>{JSON.stringify(data, null, 4)}</pre>
          </div>
    </div>
  );
};

export default App;